import { SingUpComponent } from './components/singUp/singUp.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SingInComponent } from './components/singIn/singIn.component';
import { HomeComponent } from './components/home/home.component';
import { CardOrganizationComponent } from './components/card-organization/card-organization.component';
import { CardGroupsComponent } from './components/card-groups/card-groups.component';
import { ManagementOrganizationComponent } from './components/management-organization/management-organization.component';
import { ManagmentMilestoneComponent } from './components/managment-milestone/managment-milestone.component';

const routes: Routes = [
  { path: 'signIn', component: SingInComponent},
  { path: '', component: SingInComponent},
  { path: 'signUp', component: SingUpComponent},
  { path: 'home', component: HomeComponent},
  { path: 'groups', component: CardGroupsComponent},
  { path: 'organization', component: ManagementOrganizationComponent},
  { path: 'milestone', component: ManagmentMilestoneComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
