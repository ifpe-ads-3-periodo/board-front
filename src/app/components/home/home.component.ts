import { FormBuilder, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import  axios  from 'axios';
import { Router } from '@angular/router';
import { OrganizationsService } from 'src/services/organizations.service';

const instance = axios.create();
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  token = localStorage.getItem('currentUserToken');
  orgs:any = [];
  hasOrganization:boolean  = false;
  OrganizationOrGroup: boolean = false;
  hasGroup: boolean= false;
  createOrganizationform: FormGroup;
  createGroupform: FormGroup;


  constructor(private fb: FormBuilder,private organizationsService: OrganizationsService) {
    this.createOrganizationform = {} as FormGroup;
    this.createGroupform = {} as FormGroup;
  }

  onCadastroOrganizacao(){
    let nameOrganizationToSend:string = this.createOrganizationform.get('nameOrganization')?.value;
    let sessionToken:string = localStorage.getItem('currentUserToken') as string;
    this.organizationsService.createOrganization(nameOrganizationToSend,sessionToken)
  }

  onCadastroGrupo(){
    let nameOrganizationToSend:string = this.createOrganizationform.get('nameOrganization')?.value;
    let sessionToken:string = localStorage.getItem('currentUserToken') as string;
    this.organizationsService.createOrganization(nameOrganizationToSend,sessionToken)
  }

  ngOnInit(): void {
    this.createOrganizationform = this.fb.group({
      nameOrganization: [],
    });
    this.createGroupform = this.fb.group({
      nameGroup: [],
    });

    let sessionToken:string = localStorage.getItem('currentUserToken') as string;
    this.orgs = this.organizationsService.listOrganizations(sessionToken);
    this.orgs.push("")
    console.log(this.orgs)
    console.log(this.orgs.length)
    if(this.orgs != undefined && this.orgs.length > 0){
      this.orgs.pop("")
      this.hasOrganization = true;
    }
  }



}
