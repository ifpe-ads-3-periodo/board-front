/* eslint-disable @angular-eslint/component-selector */
import { IUserLogin } from './../../interfaces/IUserLogin';
import { SingInService } from './../../../services/singIn.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-SingIn',
  templateUrl: './singIn.component.html',
  styleUrls: ['./singIn.component.scss'],
})
export class SingInComponent implements OnInit {
  singInForm: FormGroup;
  userLogin: IUserLogin;

  constructor(private fb: FormBuilder, private SingInService: SingInService) {
    this.singInForm = {} as FormGroup;
    this.userLogin = {} as IUserLogin;
  }

  ngOnInit() {
    this.singInForm = this.fb.group({
      dS_Email: [],
      dS_Password: [],
    });
  }

  onLogin() {
    this.userLogin.dS_Email = this.singInForm.get('dS_Email')?.value;
    this.userLogin.dS_Password = this.singInForm.get('dS_Password')?.value;

    this.SingInService.loginUser(this.userLogin);
  }
}
