import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-card-groups',
  templateUrl: './card-groups.component.html',
  styleUrls: ['./card-groups.component.scss']
})
export class CardGroupsComponent implements OnInit {
  groups: any = [
    {name: "Grupo 1", organization: "Zezinho"},
    {name: "Grupo 2", organization: "Maria"},
    {name: "Grupo 3", organization: "IFPE"},
    {name: "Grupo 4", organization: "Chambra"},
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
