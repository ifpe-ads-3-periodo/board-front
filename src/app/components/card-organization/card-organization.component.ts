import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'card-organization',
  templateUrl: './card-organization.component.html',
  styleUrls: ['./card-organization.component.scss']
})
export class CardOrganizationComponent implements OnInit {
  @Input() org:any = {
    
  };
  constructor() { }

  ngOnInit(): void {
  }

}
