import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-management-organization',
  templateUrl: './management-organization.component.html',
  styleUrls: ['./management-organization.component.scss']
})
export class ManagementOrganizationComponent implements OnInit {
  groups:any = [
    {nome: 'grupo1'},
    {nome: 'grupo2'},
    {nome: 'grupo3'},
    {nome: 'grupo4'},
    {nome: 'grupo5'},
    {nome: 'grupo6'},
    {nome: 'grupo7'},
  ];

  milestones:any = [
    {nome: 'Milestone 1'},
    {nome: 'Milestone 2'},
    {nome: 'Milestone 3'},
    {nome: 'Milestone 4'},
    {nome: 'Milestone 5'},
    {nome: 'Milestone 6'},
    {nome: 'Milestone 7'},
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
