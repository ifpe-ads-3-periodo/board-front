import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementOrganizationComponent } from './management-organization.component';

describe('ManagementOrganizationComponent', () => {
  let component: ManagementOrganizationComponent;
  let fixture: ComponentFixture<ManagementOrganizationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagementOrganizationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementOrganizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
