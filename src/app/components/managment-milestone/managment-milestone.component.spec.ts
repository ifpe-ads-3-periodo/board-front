import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagmentMilestoneComponent } from './managment-milestone.component';

describe('ManagmentMilestoneComponent', () => {
  let component: ManagmentMilestoneComponent;
  let fixture: ComponentFixture<ManagmentMilestoneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagmentMilestoneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagmentMilestoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
