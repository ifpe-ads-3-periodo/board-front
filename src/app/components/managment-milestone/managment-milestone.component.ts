import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-managment-milestone',
  templateUrl: './managment-milestone.component.html',
  styleUrls: ['./managment-milestone.component.scss']
})
export class ManagmentMilestoneComponent implements OnInit {
  members:any = [
    {nome: 'Fulano'},
    {nome: 'José'},
    {nome: 'Clarisvaldo'},
    {nome: 'Bruno'},
    {nome: 'Adilson'},
    {nome: 'Pedro'},
    {nome: 'Marcelo'},
  ];

  issues:any = [
    {nome: 'Issue 1'},
    {nome: 'Issue 2'},
    {nome: 'Issue 3'},
    {nome: 'Issue 4'},
    {nome: 'Issue 5'},
    {nome: 'Issue 6'},
    {nome: 'Issue 7'},
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
