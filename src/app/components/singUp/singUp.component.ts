import { IUser } from './../../interfaces/IUser';
import { User } from './../../models/user';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { SingUpService } from '../../../services/singUp.service';

@Component({
  selector: 'app-SingUp',
  templateUrl: './singUp.component.html',
  styleUrls: ['./singUp.component.scss'],
})
export class SingUpComponent implements OnInit {
  singUpForm: FormGroup;
  registeUser: IUser;

  constructor(private SingUpService: SingUpService, private fb: FormBuilder) {
    this.singUpForm = {} as FormGroup;
    this.registeUser = {} as IUser;
  }

  ngOnInit() {
    this.singUpForm = this.fb.group({
      dS_Email: [],
      dS_Password: [],
      dS_Name: [],
    });
  }

  onSubmit() {
    this.registeUser.dS_name = this.singUpForm.get('dS_Name')?.value;
    this.registeUser.dS_password = this.singUpForm.get('dS_Password')?.value;
    this.registeUser.dS_email = this.singUpForm.get('dS_Email')?.value;

    this.SingUpService.registeUser(this.registeUser);
  }
}
