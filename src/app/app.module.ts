import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';

//components region
import { NavComponent } from './components/nav/nav.component';
import { SingUpComponent } from './components/singUp/singUp.component';
import { SingInComponent } from './components/singIn/singIn.component';
import { HomeComponent } from './components/home/home.component';
import { CardOrganizationComponent } from './components/card-organization/card-organization.component';
import { CardGroupsComponent } from './components/card-groups/card-groups.component';
import { ManagementOrganizationComponent } from './components/management-organization/management-organization.component';
import { ManagmentMilestoneComponent } from './components/managment-milestone/managment-milestone.component';
//and region
@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    SingUpComponent,
    SingInComponent,
    HomeComponent,
    CardOrganizationComponent,
    CardGroupsComponent,
    ManagementOrganizationComponent,
    ManagmentMilestoneComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
