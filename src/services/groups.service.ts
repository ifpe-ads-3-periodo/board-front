import { Injectable } from '@angular/core';
import axios from 'axios';
import { Router } from '@angular/router';

const instance = axios.create();
@Injectable({
  providedIn: 'root',
})
export class GroupsService {
  constructor(private router: Router) {}

  listGroups = (userToken: string) => {
    const config = {
      headers: {
        token: userToken,
      },
    };

    let organizations: any = [];
    let prom = instance.get('http://localhost:8080/user-group/', config);

    prom.then((response) => {
      organizations.push(...response.data);
    });

    return organizations;
  };

  createGroups = (groupName: String, userToken: string) => {
    const config = {
      headers: {
        token: userToken,
      },
    };

    let organizations: any = [];
    let promCreate = instance.post(
      'http://localhost:8080/groups',
      {
        name: groupName,
      },
      config
    );

    promCreate.then((response) => {
    });
  };
}
