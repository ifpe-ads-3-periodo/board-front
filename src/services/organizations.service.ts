import { Injectable } from '@angular/core';
import axios from 'axios';
import { Router } from '@angular/router';

const instance = axios.create();
@Injectable({
  providedIn: 'root',
})
export class OrganizationsService {
  constructor(private router: Router) {}

  listOrganizations = (userToken: string) => {
    const config = {
      headers: {
        token: userToken,
      },
    };

    let organizations: any = [];
    let prom = instance.get('http://localhost:8080/organization', config);

    prom.then((response) => {
      organizations.push(...response.data);
    });

    return organizations;
  };

  createOrganization = (organizationName: String, userToken: string) => {
    const config = {
      headers: {
        token: userToken,
      },
    };

    let organizations: any = [];
    let promCreate = instance.post(
      'http://localhost:8080/organization',
      {
        name: organizationName,
      },
      config
    );

    promCreate.then((response) => {
      //fecha o modal e atualiza as organizations na tela
    });
  };
}
