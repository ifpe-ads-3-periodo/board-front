import { IUserLogin } from './../app/interfaces/IUserLogin';
import { Injectable } from '@angular/core';
import  axios  from 'axios';
import { Router } from '@angular/router';

const instance = axios.create();
@Injectable({
  providedIn: 'root',
})
export class SingInService {
  constructor(private router: Router) {}

  loginUser(user: IUserLogin) {
    console.log(user)
    instance.post("http://localhost:8080/user/login",{
      email: user.dS_Email,
      password: user.dS_Password
    }).then((response)=>{
      let sessionToken = response.data;
      console.log(sessionToken)
      localStorage.setItem('currentUserToken', sessionToken);
      this.router.navigate(['/home'])
    })
  }
}
